package edu.uw.mislagle.lifecounter;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    static final String STATE_P1_SCORE = "p1Score";
    static final String STATE_P2_SCORE = "p2Score";
    static final String STATE_P3_SCORE = "p3Score";
    static final String STATE_P4_SCORE = "p4Score";
    static final String STATE_WHO_LOST = "whoLost";

    private int p1_score = 20;
    private int p2_score = 20;
    private int p3_score = 20;
    private int p4_score = 20;
    private String whoLost = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState != null) {
            p1_score = savedInstanceState.getInt(STATE_P1_SCORE);
            p2_score = savedInstanceState.getInt(STATE_P2_SCORE);
            p3_score = savedInstanceState.getInt(STATE_P3_SCORE);
            p4_score = savedInstanceState.getInt(STATE_P4_SCORE);

            whoLost = savedInstanceState.getString(STATE_WHO_LOST);
        }

        final TextView player_loses = (TextView) findViewById(R.id.player_loses);

        final TextView p1_scoreView = (TextView) findViewById(R.id.player1_score);
        final Button p1_add5 = (Button) findViewById(R.id.p1_btn_up5);
        final Button p1_min5 = (Button) findViewById(R.id.p1_btn_dn5);
        final Button p1_add1 = (Button) findViewById(R.id.p1_btn_up1);
        final Button p1_min1 = (Button) findViewById(R.id.p1_btn_dn1);

        final TextView p2_scoreView = (TextView) findViewById(R.id.player2_score);
        final Button p2_add5 = (Button) findViewById(R.id.p2_btn_up5);
        final Button p2_min5 = (Button) findViewById(R.id.p2_btn_dn5);
        final Button p2_add1 = (Button) findViewById(R.id.p2_btn_up1);
        final Button p2_min1 = (Button) findViewById(R.id.p2_btn_dn1);

        final TextView p3_scoreView = (TextView) findViewById(R.id.player3_score);
        final Button p3_add5 = (Button) findViewById(R.id.p3_btn_up5);
        final Button p3_min5 = (Button) findViewById(R.id.p3_btn_dn5);
        final Button p3_add1 = (Button) findViewById(R.id.p3_btn_up1);
        final Button p3_min1 = (Button) findViewById(R.id.p3_btn_dn1);

        final TextView p4_scoreView = (TextView) findViewById(R.id.player4_score);
        final Button p4_add5 = (Button) findViewById(R.id.p4_btn_up5);
        final Button p4_min5 = (Button) findViewById(R.id.p4_btn_dn5);
        final Button p4_add1 = (Button) findViewById(R.id.p4_btn_up1);
        final Button p4_min1 = (Button) findViewById(R.id.p4_btn_dn1);

        p1_scoreView.setText("Total Score: " + p1_score);
        p2_scoreView.setText("Total Score: " + p2_score);
        p3_scoreView.setText("Total Score: " + p3_score);
        p4_scoreView.setText("Total Score: " + p4_score);
        player_loses.setText(whoLost);

        View.OnClickListener ocl = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(v.equals(p1_add1)){
                    p1_score += 1;
                } else if (v.equals(p1_add5)){
                    p1_score += 5;
                } else if (v.equals(p1_min1)){
                    p1_score -= 1;
                } else if (v.equals(p1_min5)){
                    p1_score -= 5;
                }
                else if(v.equals(p2_add1)){
                    p2_score += 1;
                } else if (v.equals(p2_add5)){
                    p2_score += 5;
                } else if (v.equals(p2_min1)){
                    p2_score -= 1;
                } else if (v.equals(p2_min5)){
                    p2_score -= 5;
                }
                else if(v.equals(p3_add1)){
                    p3_score += 1;
                } else if (v.equals(p3_add5)){
                    p3_score += 5;
                } else if (v.equals(p3_min1)){
                    p3_score -= 1;
                } else if (v.equals(p3_min5)){
                    p3_score -= 5;
                }
                else if(v.equals(p4_add1)){
                    p4_score += 1;
                } else if (v.equals(p4_add5)){
                    p4_score += 5;
                } else if (v.equals(p4_min1)){
                    p4_score -= 1;
                } else if (v.equals(p4_min5)){
                    p4_score -= 5;
                }

                if(p1_score > 0){
                    p1_scoreView.setText("Total Score: " + p1_score);
                } else {
                    p1_score = 0;
                    p1_scoreView.setText("Total Score: 0");
                    player_loses.setText("Player 1 LOSES!");
                    whoLost = "Player 1 LOSES!";
                }
                if(p2_score > 0) {
                    p2_scoreView.setText("Total Score: " + p2_score);
                } else {
                    p2_score = 0;
                    p2_scoreView.setText("Total Score: 0");
                    player_loses.setText("Player 2 LOSES!");
                    whoLost = "Player 2 LOSES!";
                }
                if(p3_score > 0) {
                    p3_scoreView.setText("Total Score: " + p3_score);
                } else {
                    p3_score = 0;
                    p3_scoreView.setText("Total Score: 0");
                    player_loses.setText("Player 3 LOSES!");
                    whoLost = "Player 3 LOSES!";
                }
                if(p4_score > 0) {
                    p4_scoreView.setText("Total Score: " + p4_score);
                } else {
                    p4_score = 0;
                    p4_scoreView.setText("Total Score: 0");
                    player_loses.setText("Player 4 LOSES!");
                    whoLost = "Player 4 LOSES!";
                }
            }
        };

        p1_add5.setOnClickListener(ocl);
        p1_min5.setOnClickListener(ocl);
        p1_add1.setOnClickListener(ocl);
        p1_min1.setOnClickListener(ocl);

        p2_add5.setOnClickListener(ocl);
        p2_min5.setOnClickListener(ocl);
        p2_add1.setOnClickListener(ocl);
        p2_min1.setOnClickListener(ocl);

        p3_add5.setOnClickListener(ocl);
        p3_min5.setOnClickListener(ocl);
        p3_add1.setOnClickListener(ocl);
        p3_min1.setOnClickListener(ocl);

        p4_add5.setOnClickListener(ocl);
        p4_min5.setOnClickListener(ocl);
        p4_add1.setOnClickListener(ocl);
        p4_min1.setOnClickListener(ocl);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        savedInstanceState.putInt(STATE_P1_SCORE, p1_score);
        savedInstanceState.putInt(STATE_P2_SCORE, p2_score);
        savedInstanceState.putInt(STATE_P3_SCORE, p3_score);
        savedInstanceState.putInt(STATE_P4_SCORE, p4_score);
        savedInstanceState.putString(STATE_WHO_LOST, whoLost);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
